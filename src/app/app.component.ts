import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  // annonces
  ads: any[] = [
    {
      'title': 'Une annonce',
      'description': 'Bla bla bla bla bla',
      'price': 32.99,
      'picture': 'https://images.unsplash.com/photo-1536006222476-a83275cfa5b8?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8bf7a9b84af4ff9afed74f6572d37b50&auto=format&fit=crop&w=1350&q=80',
      'status': 'En ligne',
    }
  ];

  constructor() {

  }


}
